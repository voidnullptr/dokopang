# README #

### What is this repository for? ###

* DokoPang is a very basic 3 match liner game done with the Unreal Engine 4 and C++
* The main purpose of this prototype is to show off my skills using the Unreal Engine 4 and C++

### How to play ###

Start the game and click on a tile using the mouse cursor to start sweeping for tiles of the same color, once you have 3 or more in succession you can release the mouse to accumulate points, you can also clear a highlight by drawing backwards on previous tiles, the game ends after 1 min, every time you have a match, tiles from the top drop down to where these previous tiles were and new tiles are spawned from the top.

### Who do I talk to? ###

* Contact: sonderblohm.dev@gmail.com