// Property of Erick Pombo Sonderblohm

#pragma once

#include "Engine.h"

void SimplePrintToScreen(int32 Key, float TimeToDisplay, FColor Color, wchar_t* Text);