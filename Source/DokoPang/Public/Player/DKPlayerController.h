// Property of Erick Pombo Sonderblohm

#pragma once

#include "GameFramework/PlayerController.h"
#include "DKPlayerController.generated.h"

class ADKTile;
class UUserWidget;
class UInGameScore;

/**
 * Player Controller for Doko Pang, it handles input check and matching of the tiles
 */
UCLASS()
class DOKOPANG_API ADKPlayerController : public APlayerController
{
	GENERATED_BODY()
	
	
public:

	ADKPlayerController();

	void SetSweepState(bool HasSweepStarted);
	bool GetHasSweepStarted() const { return bHasSweepStarted; }

	void AddTile(ADKTile* Tile);
	void RemoveTile(ADKTile* Tile);
	TArray<ADKTile*> GetMatchTiles() const { return MatchTiles; }
	ADKTile* GetLastMatchedTile() const;

	bool IsTileSweeped(ADKTile* Tile);
	void ShowGameOverScreen();
	void HideGameOverScreen();
	void HideInGameUIScreen();
	
protected:
	virtual void BeginPlay() override;
	virtual void SetupInputComponent() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UI")
	TSubclassOf<UUserWidget> WP_GameStart;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UI")
	TSubclassOf<UInGameScore> WP_InGameUI;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UI")
	TSubclassOf<UUserWidget> WP_GameOver;

	UPROPERTY()
	UUserWidget* WI_GameStart = nullptr;

	UPROPERTY()
	UInGameScore* WI_InGameUI = nullptr;

	UPROPERTY()
	UUserWidget* WI_GameOver = nullptr;

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	float GameTimeInSecs = 60.0f;

private:

	FString GameStart_Map = "GameStart";
	FString Main_Map = "Main";

	bool bHasSweepStarted = false;

	UPROPERTY(VisibleAnywhere, Category = "Gameplay")
	TArray<ADKTile*> MatchTiles;

	FTimerHandle GameTimerHandle;

	void InputAction_MouseDown();
	void InputAction_MouseUp();
	void ShowWidgetOnBeginPlay();
	void UpdateGameScore(int32 TilesMatchedAmount);
	bool IsGameOver() const;

};
