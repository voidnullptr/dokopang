// Property of Erick Pombo Sonderblohm

#pragma once

#include "GameFramework/Pawn.h"
#include "DKPlayerPawn.generated.h"

UCLASS()
class DOKOPANG_API ADKPlayerPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ADKPlayerPawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

private:

	UPROPERTY(VisibleAnywhere, Category = "Setup")
	UCameraComponent* PlayerCamera = nullptr;

	void InputAction_MouseClick();
	
};
