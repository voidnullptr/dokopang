// Property of Erick Pombo Sonderblohm

#pragma once

#include "Components/ActorComponent.h"
#include "SpawnComponent.generated.h"

UENUM()
enum class ESpawnState : uint8
{
	Spawning,
	DeSpawning
};

class UPaperFlipbookComponent;

/**
 *	Component that affects alpha values of the FlipBookComponent's Sprite
 *	to blend in and blend out the sprite
 */
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DOKOPANG_API USpawnComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	USpawnComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category = "Setup")
	void Initialise(UPaperFlipbookComponent* FlipBookComponentToSet);

	void SpawnTile();
	void DeSpawnTile();

private:

	FLinearColor SpriteColor{1.0f, 1.0f, 1.0f, 1.0f};

	UPROPERTY(VisibleAnywhere, Category = "Components")
	UPaperFlipbookComponent* FlipBookComponent = nullptr;

	UPROPERTY()
	UTimelineComponent* SpawnTimeline;

	UPROPERTY()
	UCurveFloat* SpawnCurve;

	FOnTimelineFloat InterpFunction;

	UFUNCTION()
	void TimelineFloatReturn(float val);
		
	// Tiles are shown by default, unless called the Spawn or DeSpawn methods
	ESpawnState SpawnState;
};
