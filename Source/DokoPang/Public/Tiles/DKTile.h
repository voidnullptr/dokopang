// Property of Erick Pombo Sonderblohm

#pragma once

#include "GameFramework/Actor.h"
#include "DKTile.generated.h"

class UPaperSpriteComponent;
class UPaperFlipbookComponent;

UENUM()
enum class ETileType : uint8
{
	None,
	Blue,
	Orange,
	Green,
	Purple
};

UENUM()
enum class EFrameType : uint8
{
	Normal = 1,
	Highlight = 2
};

class ADKGrid;

UCLASS()
class DOKOPANG_API ADKTile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADKTile();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, Category = "Ammo")
	int32 GetTileID() const;

	UFUNCTION(BlueprintCallable, Category = "Ammo")
	void SetTileID(int32 _TileID);

	UFUNCTION(BlueprintCallable, Category = "Setup")
	void SetGrid(ADKGrid* GridToSet);

	void GoToFrame(EFrameType FrameType, bool bFireEvents);

	ETileType GetTileType() const { return TileType; }

	void ClearHighlight();

	/** Makes the tile check on it's upper neighbor, call the falldown function of that neighbor (or spawn a new tile) and update the position of this tile */
	void FallDown(FVector NewPosition);

	/** Usually we would implement this as a BlueprintNativeEvent to let designers play some effect or animation before destroying the actor */
	/** Plays a particle effect and destroys tile */
	void DestroyTile();

private:

	UPROPERTY(VisibleAnywhere, Category = "Properties")
	int32 TileID = 0;

	UPROPERTY(EditDefaultsOnly, Category = "Properties")
	ETileType TileType = ETileType::None;

	UPROPERTY(VisibleAnywhere, Category = "Root")
	USceneComponent* DefaultSceneRoot = nullptr;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	UArrowComponent* ArrowComponent = nullptr;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	UPaperFlipbookComponent* FlipBookComponent = nullptr;

	UFUNCTION()
	void SpriteOnMouseClicked(UPrimitiveComponent* TouchedComponent, FKey ButtonPressed);

	UFUNCTION()
	void SpriteOnBeginMouseOver(UPrimitiveComponent* TouchedComponent);

	bool IsTileANeighbor(ADKTile* TileToCheck);

	/** Creates a box trace to get the upper tile, return nullptr if no one has been found */
	TArray<ADKTile*> GetUpperNeighbors();

	/** Reference to our Doko Pang Grid */
	ADKGrid* DKGrid = nullptr;
	
};
