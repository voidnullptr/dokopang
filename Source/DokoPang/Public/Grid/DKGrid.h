// Property of Erick Pombo Sonderblohm

#pragma once

#include "GameFramework/Actor.h"
#include "DKGrid.generated.h"

UENUM()
enum class GridType : uint8
{
	Classic,
	DokoPang
};

class ADKTile;

UCLASS()
class DOKOPANG_API ADKGrid : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADKGrid();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Creates a new grid based on DKGridType
	void SetupGrid();
	void SetupDokoPangGrid();
	void SpawnTileAtLocation(FVector NewLocation, int32 NewTileID);
	void RemoveAllTiles();

private:

	UPROPERTY(VisibleAnywhere, Category = "Components")
	USceneComponent* DefaultRootComponent = nullptr;

	UPROPERTY(VisibleAnywhere, Category = "Properties")
	TArray<ADKTile*> TileMap;

	UPROPERTY(EditInstanceOnly, Category = "Setup")
	TArray<TSubclassOf<ADKTile>> TileTypes;

	UPROPERTY(EditInstanceOnly, Category = "Setup")
	GridType DKGridType = GridType::DokoPang;

};
