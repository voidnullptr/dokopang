// Property of Erick Pombo Sonderblohm

#pragma once

#include "Blueprint/UserWidget.h"
#include "InGameScore.generated.h"

/**
 * 
 */
UCLASS()
class DOKOPANG_API UInGameScore : public UUserWidget
{
	GENERATED_BODY()
	
protected:

	UFUNCTION(BlueprintCallable, Category = "Time")
	float GetCountDownTime() const;

	UFUNCTION(BlueprintCallable, Category = "Score")
	int32 GetGameScore() const;
};
