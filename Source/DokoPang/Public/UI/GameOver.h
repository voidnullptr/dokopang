// Property of Erick Pombo Sonderblohm

#pragma once

#include "Blueprint/UserWidget.h"
#include "GameOver.generated.h"

/**
 * 
 */
UCLASS()
class DOKOPANG_API UGameOver : public UUserWidget
{
	GENERATED_BODY()
	
protected:

	UFUNCTION(BlueprintCallable, Category = "Score")
	float GetFinalScore() const;
	
	UFUNCTION(BlueprintCallable, Category = "Navigation")
	void PlayAgain();

	UFUNCTION(BlueprintCallable, Category = "Navigation")
	void ExitGame();
};
