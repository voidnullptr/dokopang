// Property of Erick Pombo Sonderblohm

#include "DokoPang.h"
#include "DKGrid.h"
#include "DKTile.h"
#include "PaperFlipbookComponent.h"
#include "SpawnComponent.h"

// Sets default values
ADKGrid::ADKGrid()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	DefaultRootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("DefaultRootComponent"));
	RootComponent = DefaultRootComponent;
}

// Called when the game starts or when spawned
void ADKGrid::BeginPlay()
{
	Super::BeginPlay();
	SetupGrid();
}

// Called every frame
void ADKGrid::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ADKGrid::SetupGrid()
{
	switch (DKGridType)
	{
		case GridType::Classic:
			break;
		case GridType::DokoPang:
			SetupDokoPangGrid();
			break;
	}
}

// Spawn Tiles in a DokoPang Grid style
void ADKGrid::SetupDokoPangGrid()
{
	// How many TileTypes do we have?
	auto NumOfTiles = TileTypes.Num();

	// Make sure we have at least a tile to spawn, if not throw an exception on the console
	if (!ensure(NumOfTiles > 0)) { return; }

	// DokoPang Grid consists of 7 x 6 grid
	int32 RowsAmount = 6;
	int32 ColumnsAmount = 7;
	int32 tileID = 0;

	// Spawn tiles for each row, from left to right
	for (int32 i = 1; i <= RowsAmount; ++i)
	{
		for (int32 j = 1; j <= ColumnsAmount; ++j)
		{
			// Spawn a random tile
			auto randomIndex = FMath::RandHelper(NumOfTiles);
			auto TileBP = TileTypes[randomIndex];
			ADKTile* Tile = GetWorld()->SpawnActor<ADKTile>(TileBP->GetOwnerClass(), GetActorLocation(), GetActorRotation());

			// Calculate sprite component bounds
			UPaperFlipbookComponent* SpriteComponent = Tile->FindComponentByClass<UPaperFlipbookComponent>();
			if (!ensure(SpriteComponent)) { return; }

			FBoxSphereBounds SpriteCompBounds = SpriteComponent->Bounds;
			FVector SpriteBoxExtent = SpriteCompBounds.BoxExtent;

			// Even tiles go in a different height
			bool bIsEven = j % 2 == 0;

			// Adding an offset since my sprite was not made as a perfect hexagon, I have to calculate this manually and use this magic number as a workaround
			float YOffset = 12.f * j;
			float YPos = ((SpriteBoxExtent.Y * 2) * j) - YOffset;
			float ZPos = bIsEven ? ((SpriteBoxExtent.Z * 2) * i) + SpriteBoxExtent.Z : (SpriteBoxExtent.Z * 2) * i;
			FVector NewLocation = GetActorLocation() + FVector(0.f, YPos, -ZPos);

			// Set new location
			Tile->SetActorLocation(NewLocation);

			// Spawn component to add a "spawn" effect
			USpawnComponent* SpawnComponent = Tile->FindComponentByClass<USpawnComponent>();
			if (SpawnComponent)
			{
				//SpawnComponent->SpawnTile();
			}

			// Reference our grid actor
			Tile->SetGrid(this);

			// Add Tile to our Grid
			Tile->SetTileID(tileID);
			TileMap.Add(Tile);
			++tileID;
		}
	}
}

void ADKGrid::SpawnTileAtLocation(FVector NewLocation, int32 NewTileID)
{
	// How many TileTypes do we have?
	auto NumOfTiles = TileTypes.Num();

	// Make sure we have at least a tile to spawn, if not throw an exception on the console
	if (!ensure(NumOfTiles > 0)) { return; }

	// Spawn a random tile
	auto randomIndex = FMath::RandHelper(NumOfTiles);
	auto TileBP = TileTypes[randomIndex];
	ADKTile* Tile = GetWorld()->SpawnActor<ADKTile>(TileBP->GetOwnerClass(), NewLocation, GetActorRotation());

	// Add Tile to our Grid
	Tile->SetTileID(NewTileID);

	// Reference our grid actor
	Tile->SetGrid(this);
	TileMap.Add(Tile);
}

void ADKGrid::RemoveAllTiles()
{
	TileMap.Empty();
}

