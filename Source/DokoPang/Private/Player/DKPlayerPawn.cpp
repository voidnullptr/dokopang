// Property of Erick Pombo Sonderblohm

#include "DokoPang.h"
#include "DKPlayerPawn.h"


// Sets default values
ADKPlayerPawn::ADKPlayerPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	PlayerCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PlayerCamera"));
	PlayerCamera->SetProjectionMode(ECameraProjectionMode::Orthographic);
	SetRootComponent(PlayerCamera);

}

// Called when the game starts or when spawned
void ADKPlayerPawn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ADKPlayerPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ADKPlayerPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction("MouseClick", IE_Pressed, this, &ADKPlayerPawn::InputAction_MouseClick);
}

void ADKPlayerPawn::InputAction_MouseClick()
{
	
}

