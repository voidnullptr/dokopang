// Property of Erick Pombo Sonderblohm

#include "DokoPang.h"
#include "DKPlayerController.h"
#include "DKTile.h"
#include "Runtime/UMG/Public/UMG.h"
#include "InGameScore.h"
#include "DokoPangGameModeBase.h"

ADKPlayerController::ADKPlayerController()
{

}

void ADKPlayerController::BeginPlay()
{
	Super::BeginPlay();
	ShowWidgetOnBeginPlay();

	bShowMouseCursor = true;
	bEnableClickEvents = true;
	bEnableMouseOverEvents = true;
}

void ADKPlayerController::SetSweepState(bool HasSweepStarted)
{
	bHasSweepStarted = HasSweepStarted;
}

void ADKPlayerController::AddTile(ADKTile* Tile)
{
	MatchTiles.Add(Tile);
}

void ADKPlayerController::RemoveTile(ADKTile* Tile)
{
	if (MatchTiles.Num() > 0)
	{
		for (ADKTile* MatchedTile : MatchTiles)
		{
			if (MatchedTile->GetTileID() == Tile->GetTileID())
			{
				MatchTiles.RemoveSingle(Tile);
				break;
			}
		}
	}
}

ADKTile* ADKPlayerController::GetLastMatchedTile() const
{
	int32 lastIndex = MatchTiles.Num() - 1;
	return MatchTiles[lastIndex];
}

bool ADKPlayerController::IsTileSweeped(ADKTile* Tile)
{
	for (auto TileIter : MatchTiles)
	{
		if (Tile->GetTileID() == TileIter->GetTileID())
		{
			return true;
		}
	}
	return false;
}

void ADKPlayerController::ShowGameOverScreen()
{
	if (WP_GameOver)
	{
		// If we have not created an instance of this widget already
		if (!WI_GameOver)
		{
			// Create widget
			WI_GameOver = CreateWidget<UUserWidget>(this, WP_GameOver);

			// Add to viewport
			if (!WI_GameOver->GetIsVisible())
			{
				WI_GameOver->AddToViewport();
				HideInGameUIScreen();
			}
		}
	}
}

void ADKPlayerController::HideGameOverScreen()
{
	if (WI_GameOver)
	{
		WI_GameOver->RemoveFromViewport();
	}
}

void ADKPlayerController::HideInGameUIScreen()
{
	if (WI_InGameUI)
	{
		WI_InGameUI->RemoveFromViewport();
	}
}

void ADKPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
	
	InputComponent->BindAction("MouseClick", IE_Pressed, this, &ADKPlayerController::InputAction_MouseDown);
	InputComponent->BindAction("MouseClick", IE_Released, this, &ADKPlayerController::InputAction_MouseUp);
}

void ADKPlayerController::InputAction_MouseDown()
{
	//SimplePrintToScreen(-1, 3.f, FColor::Green, TEXT("InputAction_MouseDown"));
}

void ADKPlayerController::InputAction_MouseUp()
{
	//SimplePrintToScreen(-1, 3.f, FColor::Red, TEXT("InputAction_MouseUp"));
	if (!IsGameOver() && MatchTiles.Num() >= 3)
	{
		// Implement behavior of new spawned tiles on the Grid
		for (ADKTile* Tile : MatchTiles)
		{
			FVector FallDownLocation = Tile->GetActorLocation() + FVector(0.f, 0.f, -50.f);
			Tile->FallDown(FallDownLocation);
			Tile->DestroyTile();
			SetSweepState(false);
		}

		// Update Game Score depending on the amount of tiles matched
		UpdateGameScore(MatchTiles.Num());
	}
	else
	{
		for (ADKTile* Tile : MatchTiles)
		{
			Tile->ClearHighlight();
			SetSweepState(false);
		}
	}
	MatchTiles.Empty();
}

void ADKPlayerController::ShowWidgetOnBeginPlay()
{
	// SimplePrintToScreen(-1, 3.f, FColor::Red, TEXT("NEW LEVEL!"));
	FString LN = UGameplayStatics::GetCurrentLevelName(this);
	
	if (LN == GameStart_Map)
	{
		if (WP_GameStart)
		{
			// If we have not created an instance of this widget already
			if (!WI_GameStart)
			{
				// Create widget
				WI_GameStart = CreateWidget<UUserWidget>(this, WP_GameStart);

				// Add to viewport
				if (!WI_GameStart->GetIsVisible())
				{
					WI_GameStart->AddToViewport();
				}
			}
		}
	}
	else if (LN == Main_Map)
	{
		if (WP_InGameUI)
		{
			// If we have not created an instance of this widget already
			if (!WI_InGameUI)
			{
				// Create widget
				WI_InGameUI = CreateWidget<UInGameScore>(this, WP_InGameUI);
			}

			// Add to viewport
			if (WI_InGameUI && !WI_InGameUI->GetIsVisible())
			{
				WI_InGameUI->AddToViewport();
				HideGameOverScreen();
			}
		}
	}
}

void ADKPlayerController::UpdateGameScore(int32 TilesMatchedAmount)
{
	// Get the game mode
	AGameModeBase* GM = UGameplayStatics::GetGameMode(this);
	ADokoPangGameModeBase* DKGM = Cast<ADokoPangGameModeBase>(GM);

	if (DKGM)
	{
		// Add points
		DKGM->AddScorePoints(TilesMatchedAmount);
	}
}

bool ADKPlayerController::IsGameOver() const
{
	// Get the game mode
	AGameModeBase* GM = UGameplayStatics::GetGameMode(this);
	ADokoPangGameModeBase* DKGM = Cast<ADokoPangGameModeBase>(GM);

	if (DKGM)
	{
		return DKGM->GetIsGameOver();
	}
	return false;
}
