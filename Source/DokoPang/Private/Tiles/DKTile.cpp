// Property of Erick Pombo Sonderblohm

#include "DokoPang.h"
#include "DKTile.h"
#include "PaperFlipbookComponent.h"
#include "DKPlayerController.h"
#include "DKGrid.h"

#define TILECHANNEL ECC_GameTraceChannel1

// Sets default values
ADKTile::ADKTile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	DefaultSceneRoot = CreateDefaultSubobject<USceneComponent>(TEXT("DefaultSceneRoot"));
	RootComponent = DefaultSceneRoot;

	ArrowComponent = CreateDefaultSubobject<UArrowComponent>(TEXT("ArrowComponent"));
	ArrowComponent->AttachToComponent(DefaultSceneRoot, FAttachmentTransformRules::KeepRelativeTransform);

	FlipBookComponent = CreateDefaultSubobject<UPaperFlipbookComponent>(TEXT("FlipBookComponent"));
	FlipBookComponent->OnBeginCursorOver.AddDynamic(this, &ADKTile::SpriteOnBeginMouseOver);
	FlipBookComponent->OnClicked.AddDynamic(this, &ADKTile::SpriteOnMouseClicked);
	FlipBookComponent->SetPlayRate(0.f);
	FlipBookComponent->AttachToComponent(DefaultSceneRoot, FAttachmentTransformRules::KeepRelativeTransform);
}

// Called when the game starts or when spawned
void ADKTile::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ADKTile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

int32 ADKTile::GetTileID() const
{
	return TileID;
}

void ADKTile::SetTileID(int32 _TileID)
{
	TileID = _TileID;
}

void ADKTile::SetGrid(ADKGrid* GridToSet)
{
	DKGrid = GridToSet;
}

void ADKTile::GoToFrame(EFrameType FrameType, bool bFireEvents = false)
{
	FlipBookComponent->SetPlaybackPositionInFrames(static_cast<int32>(FrameType), bFireEvents);
}

void ADKTile::ClearHighlight()
{
	GoToFrame(EFrameType::Normal);
}

void ADKTile::FallDown(FVector NewPosition)
{
	if (!ensure(DKGrid)) { return; }

	// Check upper neighbors
	TArray<ADKTile*> UpperNeighbors = GetUpperNeighbors();

	// Get this actor location
	FVector previousTileLocation = GetActorLocation();
	int32 previousTileID = GetTileID();

	// Fall down the whole column
	if (UpperNeighbors.Num() > 0)
	{
		for (ADKTile* Tile : UpperNeighbors)
		{
			// Store properties
			FVector CurrentTileLocation = Tile->GetActorLocation();
			int32 CurrentTileID = Tile->GetTileID();

			Tile->SetActorLocation(previousTileLocation);
			Tile->SetTileID(previousTileID);

			// Update next location values
			previousTileLocation = CurrentTileLocation;
			previousTileID = CurrentTileID;
		}
	}

	// After we are done falling down, spawn a new tile
	DKGrid->SpawnTileAtLocation(previousTileLocation, previousTileID);

	// Update this tile's position
	// TODO: Add a fancy animation to move our tiles to the new position
	SetActorLocation(NewPosition);
}

void ADKTile::DestroyTile()
{
	Destroy();
}

void ADKTile::SpriteOnMouseClicked(UPrimitiveComponent* TouchedComponent, FKey ButtonPressed)
{
	//PrintToScreen(-1, 3.f, FColor::Green, TEXT("SpriteOnMouseClicked"));
	
	// Grab PC and set bHasSweepStarted
	auto PC = GetWorld()->GetFirstPlayerController();
	if (PC)
	{
		ADKPlayerController* DKPC = Cast<ADKPlayerController>(PC);
		if (DKPC)
		{
			DKPC->SetSweepState(true);

			// Add Tile to our MatchTiles
			DKPC->AddTile(this);
		}
	}

	// Highlight this Tile to start sweeping
	GoToFrame(EFrameType::Highlight);
}

void ADKTile::SpriteOnBeginMouseOver(UPrimitiveComponent* TouchedComponent)
{
	//PrintToScreen(-1, 3.f, FColor::Green, TEXT("SpriteOnBeginMouseOver"));
	if (!ensure(TileType != ETileType::None)) { return; }

	// Grab PC and check if bHasSweepStarted
	auto PC = GetWorld()->GetFirstPlayerController();
	if (PC)
	{
		ADKPlayerController* DKPC = Cast<ADKPlayerController>(PC);
		if (DKPC)
		{
			bool bSweepHasStarted = DKPC->GetHasSweepStarted();
			//UE_LOG(LogTemp, Warning, TEXT("SpriteOnBeginMouseOver:: bSweepHasStarted: %s"), bSweepHasStarted ? TEXT("true") : TEXT("false"));

			// Check if this tile has not already been sweeped
			bool bHasBeenSweeped = DKPC->IsTileSweeped(this);

			if (bSweepHasStarted && !bHasBeenSweeped)
			{
				// Check last Tile is of the same color
				ADKTile* LastMatchedTile = DKPC->GetLastMatchedTile();
				if (LastMatchedTile->GetTileType() == TileType)
				{
					// Check that our last Tile is a neighbor, for this make a box trace by channel
					bool bIsNeighbor = IsTileANeighbor(LastMatchedTile);
					if (bIsNeighbor)
					{
						DKPC->AddTile(this);
						GoToFrame(EFrameType::Highlight);
					}
				}
			}
			else if (bSweepHasStarted && bHasBeenSweeped)
			{
				/// Clear highlight when drawing backwards
				// Check if this tile is the previous one before the last one
				TArray<ADKTile*> MatchTiles = DKPC->GetMatchTiles();
				int32 TilesMatched = MatchTiles.Num();
				if (TilesMatched > 1)
				{
					int32 index = MatchTiles.Num() - 2;
					ADKTile* Tile = MatchTiles[index];
					if (Tile->GetTileID() == GetTileID())
					{
						// Take the last highlighted tile and remove it from our array
						ADKTile* LastMatchedTile = DKPC->GetLastMatchedTile();
						DKPC->RemoveTile(LastMatchedTile);
						LastMatchedTile->GoToFrame(EFrameType::Normal);
					}
				}
			}
		}
	}
}

/*
	To really check if a Tile is a neighbor we must create a BoxTraceMulti (By Channel)
	then we check on the result if one of the neighbors of the tile in question is the previous tile
	that leads the sweep, we do this by using out TileID property
*/
bool ADKTile::IsTileANeighbor(ADKTile* TileToCheck)
{
	TArray<FHitResult> MultiHitResult;
	FVector StartLocation = GetActorLocation();
	FVector EndLocation = GetActorLocation() + FVector(0.1f, 0.f, 0.f);
	FVector HalfSize = FlipBookComponent->Bounds.BoxExtent + FVector(0.f, 2.0f, 2.0f);
	TArray<AActor*> ActorsToIgnore;
	
	if (UKismetSystemLibrary::BoxTraceMulti(
		this,
		StartLocation,
		EndLocation,
		HalfSize,
		FRotator(0),
		UEngineTypes::ConvertToTraceType(ECC_Visibility),
		false,
		ActorsToIgnore,
		EDrawDebugTrace::None,
		MultiHitResult,
		true))
	{
		for (FHitResult element : MultiHitResult)
		{
			// Cast element to ADKTile
			ADKTile* Tile = Cast<ADKTile>(element.GetActor());
			if (Tile)
			{
				if (Tile->GetTileID() == TileToCheck->GetTileID())
				{
					return true;
				}
			}
		}
	}
	return false;
}

TArray<ADKTile*> ADKTile::GetUpperNeighbors()
{
	TArray<ADKTile*> Tiles;
	TArray<FHitResult> HitResult;
	FVector StartLocation = GetActorLocation();
	FVector SpriteBoxExtent = FlipBookComponent->Bounds.BoxExtent;
	FVector EndLocation = GetActorLocation() + FVector(0.f, 0.f, SpriteBoxExtent.Z * 20);
	TArray<AActor*> ActorsToIgnore;

	UKismetSystemLibrary::LineTraceMulti(
		this,
		StartLocation,
		EndLocation,
		UEngineTypes::ConvertToTraceType(TILECHANNEL),
		false,
		ActorsToIgnore,
		EDrawDebugTrace::None,
		HitResult,
		true
	);
	if (HitResult.Num() > 0)
	{
		for (auto element : HitResult)
		{
			ADKTile* Tile = Cast<ADKTile>(element.GetActor());
			Tiles.Push(Tile);
		}
	}
	return Tiles;
}

