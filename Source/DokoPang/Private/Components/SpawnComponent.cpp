// Property of Erick Pombo Sonderblohm

#include "DokoPang.h"
#include "SpawnComponent.h"
#include "PaperFlipbookComponent.h"


// Sets default values for this component's properties
USpawnComponent::USpawnComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// Get the float curve
	ConstructorHelpers::FObjectFinder<UCurveFloat> CurveFloat(TEXT("/Game/Curves/SpawnCurve.SpawnCurve"));
	if (CurveFloat.Object)
	{
		SpawnCurve = CurveFloat.Object;
	}
	SpawnTimeline = CreateDefaultSubobject<UTimelineComponent>(TEXT("SpawnTimeline"));

	//Bind the Callback function for the float return value
	InterpFunction.BindUFunction(this, FName{ TEXT("TimelineFloatReturn") });
}


// Called when the game starts
void USpawnComponent::BeginPlay()
{
	Super::BeginPlay();

	//Add the float curve to the timeline and connect it to your my interpolation function
	SpawnTimeline->AddInterpFloat(SpawnCurve, InterpFunction, FName{ TEXT("SpawnInterp") });
}


// Called every frame
void USpawnComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void USpawnComponent::Initialise(UPaperFlipbookComponent* FlipBookComponentToSet)
{
	FlipBookComponent = FlipBookComponentToSet;
}

void USpawnComponent::SpawnTile()
{
	SpawnState = ESpawnState::Spawning;
	SpawnTimeline->Play();
}

void USpawnComponent::DeSpawnTile()
{
	SpawnState = ESpawnState::DeSpawning;
	SpawnTimeline->Play();
}

void USpawnComponent::TimelineFloatReturn(float val)
{
	UE_LOG(LogTemp, Warning, TEXT("USpawnComponent::TimelineFloatReturn: %f"), val);
	if (!ensure(FlipBookComponent)) { return; }

	switch (SpawnState)
	{
	case ESpawnState::Spawning:
		// Update alpha channel
		SpriteColor.A = val;
		break;
	case ESpawnState::DeSpawning:
		// Update alpha channel
		SpriteColor.A = 1 - val;
		break;
	default:
		return;
	}

	// Create a spawn effect by updating the alpha value of the sprite over time
	FlipBookComponent->SetSpriteColor(SpriteColor);
}

