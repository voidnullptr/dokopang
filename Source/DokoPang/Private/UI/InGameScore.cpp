// Property of Erick Pombo Sonderblohm

#include "DokoPang.h"
#include "InGameScore.h"
#include "DokoPangGameModeBase.h"

float UInGameScore::GetCountDownTime() const
{
	AGameModeBase* GM = UGameplayStatics::GetGameMode(this);
	ADokoPangGameModeBase* DKGM = Cast<ADokoPangGameModeBase>(GM);
	if (DKGM)
	{
		return DKGM->GetGameTimeInSeconds();
	}
	return 0.0f;
}

int32 UInGameScore::GetGameScore() const
{
	AGameModeBase* GM = UGameplayStatics::GetGameMode(this);
	ADokoPangGameModeBase* DKGM = Cast<ADokoPangGameModeBase>(GM);
	if (DKGM)
	{
		return DKGM->GetGameScore();
	}
	return 0;
}
