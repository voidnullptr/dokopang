// Property of Erick Pombo Sonderblohm

#include "DokoPang.h"
#include "GameOver.h"
#include "DokoPangGameModeBase.h"

float UGameOver::GetFinalScore() const
{
	AGameModeBase* GM = UGameplayStatics::GetGameMode(this);
	ADokoPangGameModeBase* DKGM = Cast<ADokoPangGameModeBase>(GM);
	if (DKGM)
	{
		return DKGM->GetGameScore();
	}
	return 0.0f;
}

void UGameOver::PlayAgain()
{
	AGameModeBase* GM = UGameplayStatics::GetGameMode(this);
	ADokoPangGameModeBase* DKGM = Cast<ADokoPangGameModeBase>(GM);
	if (DKGM)
	{
		DKGM->RestartDokoPang();
	}
}

void UGameOver::ExitGame()
{
	GetWorld()->GetFirstPlayerController()->ConsoleCommand("quit");
}
