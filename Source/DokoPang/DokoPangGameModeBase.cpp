// Property of Erick Pombo Sonderblohm

#include "DokoPang.h"
#include "DokoPangGameModeBase.h"
#include "DKPlayerController.h"
#include "DKGrid.h"

ADokoPangGameModeBase::ADokoPangGameModeBase()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ADokoPangGameModeBase::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (!bIsGameOver)
	{
		GameTimerInSeconds -= DeltaSeconds;

		if (GameTimerInSeconds <= 0.0f)
		{
			bIsGameOver = true;
			GameTimerInSeconds = 0.0f;
			auto PC = GetWorld()->GetFirstPlayerController();
			if (PC)
			{
				ADKPlayerController* DKPC = Cast<ADKPlayerController>(PC);
				if (DKPC)
				{
					DKPC->ShowGameOverScreen();
				}
			}
		}
	}
}

float ADokoPangGameModeBase::GetGameTimeInSeconds() const
{
	return GameTimerInSeconds;
}

void ADokoPangGameModeBase::SetIsGameOver(bool value)
{
	bIsGameOver = value;
}

bool ADokoPangGameModeBase::GetIsGameOver() const
{
	return bIsGameOver;
}

void ADokoPangGameModeBase::AddScorePoints(int32 TilesMatchedAmount)
{
	GameScore += TilesMatchedAmount * PointsPerTile;
}

int32 ADokoPangGameModeBase::GetGameScore() const
{
	return GameScore;
}

void ADokoPangGameModeBase::RestartDokoPang()
{
	// Destroy Tiles from the grid
	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ADKGrid::StaticClass(), FoundActors);
	if (FoundActors.Num() > 0)
	{
		// We only have one grid actor
		ADKGrid* Grid = Cast<ADKGrid>(FoundActors[0]);
		Grid->RemoveAllTiles();
	}

	// Open Level
	UGameplayStatics::OpenLevel(GetWorld(), "Main");
}
