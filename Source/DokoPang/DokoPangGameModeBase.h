// Property of Erick Pombo Sonderblohm

#pragma once

#include "GameFramework/GameModeBase.h"
#include "DokoPangGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class DOKOPANG_API ADokoPangGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:

	ADokoPangGameModeBase();

public:
	virtual void Tick(float DeltaSeconds) override;

	float GetGameTimeInSeconds() const;

	void SetIsGameOver(bool value);
	bool GetIsGameOver() const;

	void AddScorePoints(int32 TilesMatchedAmount);
	int32 GetGameScore() const;

	UPROPERTY(EditDefaultsOnly, Category = "Time")
	int32 PointsPerTile = 5;

	void RestartDokoPang();

private:

	bool bIsGameOver = false;

	UPROPERTY(VisibleAnywhere, Category = "Time")
	float GameTimerInSeconds = 60;

	UPROPERTY(VisibleAnywhere, Category = "Time")
	float GameScore = 0;

};
