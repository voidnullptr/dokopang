// Property of Erick Pombo Sonderblohm

#include "DokoPang.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, DokoPang, "DokoPang" );

void SimplePrintToScreen(int32 Key, float TimeToDisplay, FColor Color, wchar_t* Text)
{
	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(Key, TimeToDisplay, Color, Text);
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("Blocking Hit =: %s"), bBlockingHit));
	}
}
