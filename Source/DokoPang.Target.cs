// Property of Erick Pombo Sonderblohm

using UnrealBuildTool;
using System.Collections.Generic;

public class DokoPangTarget : TargetRules
{
	public DokoPangTarget(TargetInfo Target)
	{
		Type = TargetType.Game;
	}

	//
	// TargetRules interface.
	//

	public override void SetupBinaries(
		TargetInfo Target,
		ref List<UEBuildBinaryConfiguration> OutBuildBinaryConfigurations,
		ref List<string> OutExtraModuleNames
		)
	{
		OutExtraModuleNames.AddRange( new string[] { "DokoPang" } );
	}
}
